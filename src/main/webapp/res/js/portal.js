/*
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var portal = {};

/**
 * @typedef {Map} PageDescription
 * @memberof portal.PortalApp#
 * @description Description of the page.
 *
 * @property {String} sys - Name of the linked system.
 * @property {String} appDef - Description of the application definition as a {@link portal.PortalApp#AppDescription AppDescription}.
 * @property {String} title - Title of the page.
 * @property {String} url - URL of the page.
 * @property {Function} secFunc - Function of security of the page.
 * @property {Function} cleanup - Function of cleanup of the page.
 */

 /**
  * @typedef {Map} AppDescription
  * @memberof portal.PortalApp#
  * @description Description of the application.
  *
	* @property {Array<String>} modules - List of modules to load.
	* @property {Boolean} loaded - True if the application is loaded.
	* @property {String} navBar - The navbar description.
  * @property {String} title - Title of the application.
  */

/**
 * @typedef {Map} LoadingPage
 * @memberof portal.PortalApp#
 * @description Description of the loading page.
 *
 * @property {String} pageId - The ID of the page to load.
 * @property {Map} [args] - Arguments for the loading.
 * @property {String} pageDef - Description of the loading page as a {@link portal.PortalApp#PageDescription PageDescription}.
 * @property {String} appId - The ID of the application.
 * @property {String} sys - Name of the linked system.
 */

/**
 * @class PortalApp
 * @memberof portal
 * @description
 * The PortalApp provides an application with basic modules:
 * - An user management.
 * - Basic links of the application.
 * - The page navigation.
 */
portal.PortalApp = butor.App.extend({
	_user : null,
	_curPage : null,
	_loadingPage : null,
	_appsDef : {},
	_pagesDef : {},
	_menuDef : [],
	_firstPage : null,
	_defaultPage : "welcome",
	_appHeader : null,
	_sessionTimedOut : false,
	construct : function() {
		this.base();
		LOGGER.setLevel(LOGGER.INFO);
	},

	/**
	 * @method start
	 * @memberof portal.PortalApp#
	 * @description Start the **Portal** application.
	 */
	start : function() {
		this.base();
		this.setSessionId(butor.Utils.getUUID());

		$("#switchLangLink").click($.proxy(function(e_) {
			this.switchLangPortal();
		}, this));
		$("#defaultPageLink").click($.proxy(this._setDefaultPage, this));

		$("#exportPdfLink").click($.proxy(function() {
			var node = $("#_content");
			this.html2pdf(node, {
				'title' : $(document).attr('title')
			});
		}, this));

		$("#_brand").click($.proxy(this.goHome, this));
		$("#signInLink").click($.proxy(this._signInOut, this));
		$("#signOutLink").click($.proxy(this._signInOut, this));
		$("#manageApiKeyLink").click($.proxy(this._manageApiKey, this));
		$("#changePwdLink").click($.proxy(this._changePwd, this));
		$("#setQRsLink").click($.proxy(this._setQRsPwd, this));
		$("#userMenuItem").hide();
		$("#signInMenuItem").hide();

		$("#signInMenuItem").removeClass("hidden");
		$("#userMenuItem").removeClass("hidden");

		$("#detachLink").click($.proxy(this._detachPage, this));
		$("#contactUsLink").hide();

		this._appHeader = $("#appHeader");
		//this._onBeforeCloseP = $.proxy(function(){
		//	return this.tr('Close') +' ' +$(document).attr('title') +'?';
		//},this);
		//$(window).bind('beforeunload', this._onBeforeCloseP);
		$(window).resize($.proxy(this._resized, this));

		this._sd = false;
		this._pl = true;
		var gh = $.proxy(function() {
			if (this._sd && this._pl) {
				var url = this._bundle.get('contact-us-url');
				if (url != 'contact-us-url') {
					this._pagesDef["portal.contactUs"] = {
						'sys' : "portal",
						'secFunc' : null,
						'url' : url,
						'title' : 'Contact us'
					};
					$("#contactUsLink").click($.proxy(this._contactUs, this)).show();
				}
				// cur lang
				var lang = $.cookie("lang");
				if (butor.Utils.isEmpty(lang)) {
					lang = this.getUserProfile().get("language");
					if (butor.Utils.isEmpty(lang)) {
						lang = 'en';
					}
				}
				this.setLang(lang);
				this.translate();
				this._langChanged();

				var state = $.bbq.getState();
				if (state && state.t === 'p') {
					this.showPage(state.p, state.args);
				} else {
					this.goHome();
				}
				
				this._checkPwdExpiry();
			}
		},this);

		this.bind('setupDone', $.proxy(function(event) {
			this._buildMainMenu();
			this._sd = true;
			gh();
		},this));

		this.bind('profileLoaded', $.proxy(function(event) {
			this._pl = true;
			gh();
		},this));

		this.bind('historyChanged', $.proxy(this._goHistory, this));

		this.bind('langChanged', $.proxy(this._langChanged, this));

		this._setup();
	},
	_resized : function() {
		this._reorgMenu();
	},
	_langChanged : function() {
		$("#detachLink").attr('title', this.tr('Open in a new window'));
		this.getUserProfile().save("language", this.getLang());
		var dpreg = this.getLang();
		//TODO update datepicker instances language
//		if (dpreg == 'en') {
//			dpreg = '';
//		}
		//$.datepicker.setDefaults($.datepicker.regional[dpreg]);
		//$.datepicker.language = dpreg;//($.datepicker.regional[dpreg]);

		// adjust app menu margin
		var appTitlePanel = $('#appTitlePanel');
		this._appHeader.find("#appNavBar").css('margin-right', appTitlePanel.width());
	},
	_setDefaultPage : function() {
		this.getUserProfile().save("portal.default.page", this._curPage['pageId']);
	},
	_goHistory : function(evt) {
		var state = evt.data;
		LOGGER.info('goHistory: ' +JSON.stringify(state));
		if (!state || !state.t) {
			return;
		}
		if (state.t === 'p' && state.p != this._curPage['pageId']) {
			this.showPage(state.p, state.args);
		}
	},
	_detachPage : function() {
		window.open(window.location, '_blank', 'toolbar=0,location=0,menubar=0,resizable=1,scrollbars=1');
	},
	_contactUs : function() {
		var url = this._bundle.get('contact-us-url');
		this._pagesDef["portal.contactUs"].url = url;
		this.showPage("portal.contactUs");
		return false;
	},
	_buildMainMenu : function() {
		var menu = $("ul.main-menu");
		menu.empty();
		var index = 0;
		var aBundle = {};
		var handlers = {};
		for (var i=0,c=this._menuDef.length;i<c; i+=1) {
			var def = this._menuDef[i];
			var page = null;
			var sec = def['secFunc'];
			if (def['page'] && this._pagesDef[def['page']]) {
				page = this._pagesDef[def['page']];
				if (sec === undefined) {
					sec = {'sys':page['sys'],'func':page['secFunc']};
				} else if (page['secFunc']) {
					sec['secFunc'] = page['secFunc'];
				}
			}
			if (sec && !this.hasAccess(sec['sys'], sec['func'])) {
				continue;
			}
			index++;
			var mid = 'i' +index;
			var item = "<li style=\"xdisplay:none;\" id=\"" +mid +"\"><a class=\"top-navbar-item\">";
			if (def['icon']) {
				item += "<i class=\"" +def['icon'] +" fa fa-white\"></i> ";
			}

			var caption = '';
			if (def['caption']) {
				caption = def['caption'];
			} else if (page) {
				if (this._appsDef[page['sys']])
					caption = this._appsDef[page['sys']]['title'];
			}
			var bKey = 'menu.caption.';
			if (typeof(caption) === 'object') {
				bKey += caption['en'];
				aBundle[bKey] = caption;
			} else {
				bKey += caption;
				aBundle[bKey] = {'en' : caption, 'fr' : caption};
			}
			item += "<span class=\"bundle\">" +bKey +"</span></a></li>";

			if (!this._firstPage) {
				this._firstPage = def['page'];
			}
			item = $(item);
			menu.append(item);

			handlers[mid] = this._createMenuHandler(def);
		}
		var plus = $('<li style="xdisplay:none;" id="iplus" class="dropdown" id="moreAppsMenuItem">' +
				'<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="bundle">More</span> <b class="caret"></b></a>' +
				'<ul class="dropdown-menu"></ul></li>');
		menu.append(plus);
		this.addBundle('common', aBundle);
		this.translateElem($("ul.main-menu"));
		this._reorgMenu();
		menu.delegate('a.top-navbar-item', 'click', function() {
			var link = $(this);
			handlers[link.parent().attr('id')]();
			if ($('#_header .navbar-toggle').is(':visible')) {
				$('#_header .portal-main-menu').collapse('hide');
			}
		});
		this._toggleMenuItemsOnSec();
	},
	_toggleMenuItemsOnSec : function() {
		// Toggle menu according to user security
		this.hasAccess('sec', 'apiKey', 2) ? $("#manageApiKeyLink").parent().show() : $("#manageApiKeyLink").parent().hide();
	},
	_reorgMenu : function() {
		var collapsedNavbarToggle = $('.navbar-header button.navbar-toggle').is(":visible");

		var menu = $("ul.main-menu");
		var rightLimit = menu.next().offset().left;
		//alert('rightLimit=' +rightLimit);
		var enough = false;
		var as = rightLimit -menu.offset().left -100; //100 padding
		var items = menu.find('li').hide();
		var plusItem = menu.find('#iplus').show();
		var plusMenu = plusItem.find('.dropdown-menu');
		plusMenu.find('li').hide().remove().insertBefore(plusItem);
		items.each(function(index, elem) {
			var item = $(this);
			if (item.attr('id') == 'iplus') {
				return;
			}
			if (enough) {
				item.remove().appendTo(plusMenu);
				item.show();
				return;
			}
			item.show();
			var mw = menu.width();
			if (!collapsedNavbarToggle && mw > as) {
				enough = true;
				item.remove().appendTo(plusMenu);
				//item.show();
			}
		});
		if (plusMenu.find('li').length == 0) {
			plusItem.hide();
		}
	},
	_createMenuHandler : function(def) {
		var self = this;
		var hh = function(def) {
			var _def = def;
			if (_def['page']) {
				return function() {
					self.showPage(_def['page']);
				}
			} else if (_def['func']) {
				return function() {
					if (_def['sys']) {
						self._loadAppModules(_def['sys'], function(){
							if (_def['cmpldFunc'] === undefined) {
								_def['cmpldFunc'] = eval("0," +_def['func']);
							}
							_def['cmpldFunc']();
						});
					} else {
						if (_def['cmpldFunc'] === undefined) {
							_def['cmpldFunc'] = eval("0," +_def['func']);
						}
						_def['cmpldFunc']();
					}
				}
			} else {
				var str = "Got invalid menu item: " +JSON.stringify(def);
				LOGGER.error(str);
				return function() {
					self.showMsg({msg:str, isError:true});
				};
			}
		}
		var handler =  hh(def);
		return handler;
	},
	_loadAppModules : function(appId, doneCallback) {
		this.ping($.proxy(function(echo) {
			if (echo == null) {
				return;
			}
			var appDef = this._appsDef[appId];
			var modules = appDef['modules'];
			if (!modules || appDef['loaded']) {
				if (typeof(doneCallback) == 'function') {
					doneCallback();
				}
				return;
			}
			var loader = new butor.Loader();
			var mLoadedHandler = function (e_) {
				var data = e_.data;
				if (data['loadName'] !== appId) {
					return;
				}
				if (data['success'] === false) {
					alert('Failed to load script : ' +data['url']);
				}
				if (data['index']  === data['total']) {
					loader.unbind('scriptLoaded', mLoadedHandler);
					appDef['loaded'] = true;
					if (typeof(doneCallback) == 'function') {
						doneCallback();
					}
				}
			}
			loader.bind('scriptLoaded', mLoadedHandler);
			loader.loadScripts(modules, appId);
		},this));
	},
	_showPageStep2 : function() {
		var pageDef = this._loadingPage['pageDef'];
		var nb = this._appHeader.find("#appNavBar");
		var delayPageOpen = false;
		var appDef = this._appsDef[pageDef['sys']];
		this._loadingPage['appDef'] = appDef;
		this._loadingPage['sys'] = pageDef['sys'];

		var navBar = appDef ? appDef['navBar'] : null;
		var curApp = this._curPage && !butor.Utils.isEmpty(this._curPage['sys']) ? this._appsDef[this._curPage['sys']] : null;
		if (butor.Utils.isEmpty(curApp) || navBar != curApp.navBar || curApp.navBar == null ) {
			this._curPage && this.fire('removing-navbar', this._curPage['sys']);
			nb.empty();
			var self = this;
			if (!butor.Utils.isEmpty(navBar)) {
				delayPageOpen = true;
				nb.load(navBar, null, function() {
					self.translateElem(nb, pageDef['sys']);
					self._showPageBody();
				}).show();
				this._appHeader.removeClass('no-app-menu');
			} else {
				nb.hide();
				this._appHeader.addClass('no-app-menu');
			}

			// optional app footer
			var foot = appDef ? appDef['footer'] : null;
			var af = $("#_container > #appFooter");
			af.empty();
			if (!butor.Utils.isEmpty(foot)) {
				af.load(foot, null, function() {
					self.translateElem(af, pageDef['sys']);
				});
			}
		}

		if (!delayPageOpen) {
			this._showPageBody();
		}
	},
	_showPageBody : function(pageDef, appDef) {
		var pageDef = this._loadingPage['pageDef'];
		var appDef = this._loadingPage['appDef'];
		var appTitle = this.tr(pageDef['title'], pageDef['sys']);
		$("#appTitle").text('');
		$("#appTitle").prop('key', pageDef['title']);
		var portalTitle = this.tr('Portal');
		if (portalTitle != appTitle) {
			$(document).attr('title', portalTitle +' - ' +appTitle);
		} else {
			$(document).attr('title', portalTitle);
		}
		var appTitlePanel = $('#appTitlePanel');
		this.translateElem(appTitlePanel, pageDef['sys']);
		appTitlePanel.show();
		this._appHeader.find("#appNavBar").css('margin-right', appTitlePanel.width());

		var url = pageDef['url'];
		if (!butor.Utils.isEmpty(url)) {
			gaPush(url);

			var self = this;
			$('#_content').load(url, null, function(){
				self.translateElem($('#_content'), pageDef['sys']);
				$("#_container").unmask();
			});
		} else {
			$("#_container").unmask();
		}
		this._curPage = this._loadingPage;

		this.pushHistory({'t':'p',
			'p':this._loadingPage['pageId'],
			'args':this._loadingPage['args']},
			'', '');
	},
	_setup : function() {
		this._loadUserInfo($.proxy(function() {
			if (!this._user) {
				return;
			}
			this._user['funcs'] = {};
			AJAX.call('portal.ajax', {'streaming':false, 'service':'getSetup'},
				[], {'scope':this, 'callback':function(result) {
					if (result && !result['hasError'] && result['data'] && result['data'][0]) {
						var setup = result['data'][0];
						this._user['funcs'] = {};
						if (setup['funcs']) {
							var funcs = setup['funcs'];
							for (var i=0,c=funcs.length;i<c;i+=1) {
								var key = funcs[i]['sys'] +'.' +funcs[i]['func']
								this._user['funcs'][key] = funcs[i]['mode'];
							}
						}
						if (setup['appsDef']) {
							this._appsDef = setup['appsDef'];
						}
						if (setup['pagesDef']) {
							this._pagesDef = setup['pagesDef'];
							var aBundle = {};
							// set titles
							for (var pc in this._pagesDef) {
								var pageDef = this._pagesDef[pc];
								var appDef = this._appsDef[pageDef['sys']];
								if (!appDef) {
									appDef = {};
								}
								var pTitle = !butor.Utils.isEmpty(pageDef['title']) ? pageDef['title'] : appDef['title'];
								var bKey = 'page.title.';
								if (typeof(pTitle) === 'object') {
									bKey += pTitle['en'];
									aBundle[bKey] = pTitle;
								} else {
									bKey += pTitle;
									aBundle[bKey] = {'en' : pTitle, 'fr' : pTitle};
								}
								pageDef['title'] = bKey;
							}
							this.addBundle('common', aBundle);
						}
						if (setup['menuDef']) {
							this._menuDef = setup['menuDef'];
						}
					}

					this.fire('setupDone');
				}});
		}, this));
	},
	_loadUserInfo : function(doneHandler) {
		AJAX.call('/sso/login.ajax',
			{'streaming':false, 'service':'getUserInfo'},
			[], {'scope':this,'callback':function(result) {
				if (result && !result['hasError'] && result['data'] && result['data'][0]) {
					this._env = result['data'][0]['env'];
					this._user = result['data'][0]['user'];
					if (this._user) {
						this._user['firmId'] = result['data'][0]['firmId'];
						this._user['email'] = result['data'][0]['email'];
						this._user['twoStepsSignin'] = result['data'][0]['twoStepsSignin'];
						this._user['theme'] = result['data'][0]['theme'];
						this._user['firmName'] = result['data'][0]['firmName'];
						this._user['pwdExpiryDays'] = result['data'][0]['pwdExpiryDays'];
						this._user['profile'] = new portal.UserProfile();
						this._user['profile'].setProvider(new portal.ProfileProvider());
						this._user['profile'].load();

						this._user['twoStepsSignin'] ? $("#setQRsLink").parent().show() : $("#setQRsLink").parent().hide();
					} else {
						//session timed out
						this.sessionTimedOut();
						return;
					}
				} else {
					doneHandler();
					return;
				}
				if (this._env !== 'prod') {
					$(".footer .env").html('/' +this._env);
				} else {
					window['ga-disable-XXX-X'] = false;
				}

				if (!butor.Utils.isEmpty(this._user)) {
					var uf = this.getUser();
					$("#user").html('<i class="fa fa-user fa fa-white"></i> ' +butor.Utils.escapeStr(uf.displayName)
						+' <b class="caret">');
					$("#userMenuItem").show();
					$("#signInMenuItem").hide();


				} else {
					$("#userMenuItem").hide();
					$("#signInMenuItem").show();
				}
				doneHandler();
			}});
	},
	_signInOut : function() {
		var backUrl = "/";
		if (this.isUserKnown()) {
			//$(window).unbind('beforeunload');
			// signout
			document.location.href = '/sso/signout?service=' +backUrl;
		} else {
			// signin
			document.location.href = "/sso/index.html?service=" +backUrl;
		}
	},
	_manageApiKey : function() {
		var backUrl = "/";
		document.location.href = "/sso/index.html?m=k&service=" +backUrl;
	},
	_changePwd : function() {
		var backUrl = "/";
		document.location.href = "/sso/index.html?m=c&service=" +backUrl;
	},
	_setQRsPwd : function() {
		var backUrl = "/";
		document.location.href = "/sso/index.html?m=q&service=" +backUrl;
	},

	/**
	 * @method getId
	 * @memberof portal.PortalApp#
	 * @description
	 * Get the portal application ID.
	 * @return {String} The application ID.
	 * @example
	 * App.getId(); // output: 'portal'
	 */
	getId : function() {
		return 'portal';
	},

	/**
	 * @method isUserKnown
	 * @memberof portal.PortalApp#
	 * @description
	 * Check if the current user is known.
	 * @return {Boolean} True if the user is known.
	 */
	isUserKnown : function() {
		return this._user != null;
	},

	/**
	 * @method isUserAdmin
	 * @memberof portal.PortalApp#
	 * @description
	 * Check if the current user is an admin.
	 * @return {Boolean} True if the user is an admin.
	 */
	isUserAdmin : function() {
		//TODO
		return false;
	},

	/**
	 * @method getUser
	 * @memberof portal.PortalApp#
	 * @description
	 * Get the current user.
	 * @return {Map} The current user.
	 */
	getUser : function() {
		return this._user;
	},

	/**
	 * @method getUserProfile
	 * @memberof portal.PortalApp#
	 * @description
	 * Get the user profile.
	 * @return {Map} The user profile.
	 */
	getUserProfile : function() {
		return this._user['profile'];
	},

	/**
	 * @method hasAccess
	 * @memberof portal.PortalApp#
	 * @description
	 * Check if the user can have access to a specific system.
	 * @param {String} sys - Name of the system.
	 * @param {String} func - Name of the system function.
	 * @param {Number} [mode=1] - Authorization mode (1 for read-only access).
	 * @return {Boolean} True if the user have the acccess.
	 */
	hasAccess : function(sys, func, mode) {
		if (butor.Utils.isEmpty(func)) {
			return true;
		}
		if (butor.Utils.isEmpty(this._user) || butor.Utils.isEmpty(this._user['funcs'])) {
			return false;
		}
		var key = sys +'.' +func;
		if (mode == undefined) {
			mode = 1;//read
		}
		return (this._user['funcs'][key] >= mode);
	},

	/**
	 * @method goHome
	 * @memberof portal.PortalApp#
	 * @description
	 * Go to the home page.
	 */
	goHome : function() {
		// preferred page ?
		var pid = this.getUserProfile().get("portal.default.page");
		if (!butor.Utils.isEmpty(pid)) {
			// still have access?
			var pageDef = this._pagesDef[pid];
			if (!pageDef) {
				// page !exists or have no access
				pid = null;
			} else if (pageDef['secFunc'] && !this.hasAccess(pageDef['sys'], pageDef['secFunc'])) {
				// have no access
				pid = null;
			}
		}
		if (butor.Utils.isEmpty(pid)) {
			// show the first page in user main menu
			pid = this._firstPage;
		}
		if (butor.Utils.isEmpty(pid)) {
			// default page
			pid = this._defaultPage;
		}
		this.showPage(pid);
	},

	/**
	 * @method _checkPwdExpiry
	 * @memberof portal.PortalApp#
	 * @description
	 * Check if password will expire soon and advice user.
	 */
	_checkPwdExpiry : function() {
		var pe = this._user['pwdExpiryDays'];
		if (pe != null && pe>-1 && pe<7) {
			$("#userMenuItem").popover({'placement':'bottom', 'title':this.tr('Password'), 'trigger':'manual', 'html':true,
				'content':this.tr('pwdExpireSoon').replace('_days_', this._user['pwdExpiryDays'])})
				.popover('show');

			$("#popChangePwdLink").click($.proxy(function() {
				$("#userMenuItem").popover('destroy');
				this._changePwd();
			}, this));

			$("#dismissPopChangePwdLink").click(function() {
				$("#userMenuItem").popover('destroy');
			});
		}
	},
	
	/**
	 * @method showPage
	 * @memberof portal.PortalApp#
	 * @description
	 * Show a specific page.
	 * @fires portal.PortalApp#removing-page
	 * @fires portal.PortalApp#opening-page
	 * @param {String} pageId - The page ID to show.
	 * @param {Map} args - Argument for the page loading.
	 */
	showPage : function(pageId_, args_) {
		this.hideMsg();
		this.unmask();
		var pageDef = this._pagesDef[pageId_];
		if (!pageDef && !this._appsDef[pageId_]) {
			if (pageId_ && pageId_.indexOf('/') == 0) {
				pageDef = this._pagesDef[pageId_] = {'url':pageId_, 'sys':pageId_};
				this._appsDef[pageId_] = {};
			} else {
				this.showMsg({msg: this.tr('Page not defined') +' id=' +pageId_, isError:true});
				return;
			}
		}
		pageDef['pageId'] = pageId_;

		//cleanup current page
		if (this._curPage) {
			try {
				this.fire('removing-page', this._curPage['pageId']);

				//TODO deprecated way to clean-up
				var cleanup = this._curPage['pageDef']['cleanup'];
				if (cleanup && typeof cleanup == "function") {
					cleanup();
				}
			} catch (err) {
				//ok
			}
		}

		this._loadingPage = {
			'pageId' : pageId_,
			'args' : args_,
			'pageDef' : pageDef,
			'appId' : pageDef['sys']
		};
		this.fire("opening-page", pageId_);

		$('#_content').empty();
		$("#_container").mask(this.tr("Loading"));

		this._loadAppModules(pageDef['sys'], $.proxy(this._showPageStep2, this));
	},

	/**
	 * @method getCurPageDef
	 * @memberof portal.PortalApp#
	 * @description
	 * Get the current page definition.
	 */
	getCurPageDef : function() {
		return this._curPage ? this._curPage['pageDef'] : null;
	},

	/**
	 * @method getPageArgs
	 * @memberof portal.PortalApp#
	 * @description
	 * Get the loading page arguments.
	 */
	getPageArgs : function() {
		return this._loadingPage ? this._loadingPage['args'] : null;
	},

	/**
	 * @method registerCleanup
	 * @memberof portal.PortalApp#
	 * @description
	 * Cleanup a specific page.
	 * @param {String} pageId - The specific page ID.
	 * @param {Function} cleanupFn - The function for cleanup
	 * @deprecated.
	 */
	registerCleanup : function(pageId, cleanupFn) {
		LOGGER.warn('registerCleanup() is deprecated. Please use App.bind("removing-page").');
		if (!this._pagesDef[pageId]) {
			LOGGER.error('registerCleanup(): page with id="' +pageId +'" is not defined');
			return;
		}
		this._pagesDef[pageId].cleanup = cleanupFn;
	},

	/**
	 * @method setLang
	 * @memberof portal.PortalApp#
	 * @description
	 * Set a specific language for the application.
	 * @param {String} lang - The language to set.
	 * @see butor.Bundle#setLang
	 */
	setLang : function(lang) {
		LOGGER.info('Setting language to ' + lang + ' ...');
		$("#switchLangLink").html(
				'<i class="fa fa-language"></i> '
				+ (lang === 'fr' ? 'English' : 'Français'));

		this._bundle.setLang(lang);
	},

	/**
	 * @method switchLangPortal
	 * @memberof portal.PortalApp#
	 * @description
	 * Switch the portal language.
	 * @param {String} lang - The language to switch.
	 */
	switchLangPortal : function(lang) {
		var sys = !butor.Utils.isEmpty(this._curPage) ? this._curPage['sys'] : null;
		this.switchLang(lang, sys);
	},

	/**
	 * @method ping
	 * @memberof portal.PortalApp#
	 * @description
	 * Make an AJAX request to 'ping' service.
	 * @param {Function} doneCallback - Function to execute when the request is done.
	 */
	ping : function(doneCallback) {
		AJAX.call('portal.ajax',
			{'streaming':false, 'service':'ping'},
			[], {'scope':this,'callback':function(result) {
				if (result && !result['hasError'] && result['data'] && result['data'][0]) {
					if (typeof(doneCallback) == 'function') {
						doneCallback(result['data'][0]);
					}
				} else {
					if (typeof(doneCallback) == 'function') {
						doneCallback(null);
					}
				}
			}});
	},

	/**
	 * @method sessionTimedOut
	 * @memberof portal.PortalApp#
	 * @description
	 * Notify a 'session timeout' and disconnect the user.
	 */
	sessionTimedOut : function () {
		if (!this._sessionTimedOut) {
			this._sessionTimedOut = true;
			alert(this.tr("SESSION_TIMEDOUT"));
			//$(window).unbind('beforeunload');
			window.location = "/sso/index.html?service=/";
		}
	},

	/**
	 * @method mailTo
	 * @memberof portal.PortalApp#
	 * @description
	 * Create a mailto link from an adress
	 * @param {String} rcpts - The specified adress.
	 */
	mailTo : function(rcpts) {
		document.location.href="mailto:" +rcpts;
		/*
		try {
			$(window).unbind('beforeunload', this._onBeforeCloseP);
			document.location.href="mailto:" +rcpts;
		} finally {
			//defer
			setTimeout(function() {
				$(window).bind('beforeunload', App._onBeforeCloseP);
			},0);
		}
		*/
	},

	/**
	 * @method setTitle
	 * @memberof portal.PortalApp#
	 * @description
	 * Set a new application title.
	 * @param {String} appTitle - The title.
	 */
	setTitle : function(appTitle) {
		$("#appTitle").text('');
		$("#appTitle").prop('key', appTitle);
		var portalTitle = this.tr('Portal');
		if (portalTitle != appTitle) {
			$(document).attr('title', portalTitle +' - ' +appTitle);
		} else {
			$(document).attr('title', portalTitle);
		}
		var appTitlePanel = $('#appTitlePanel');
		this.translateElem(appTitlePanel);
		appTitlePanel.show();
		this._appHeader.find("#appNavBar").css('margin-right', appTitlePanel.width());
	},
	
	/**
	 * @method html2pdf
	 * @memberof portal.PortalApp#
	 * @description
	 * convert html to PDF.
	 * @param Element node - html element to convert to pdf.
	 * @param {Map} args - html2pdf args {'name':'<page name>', 'html':'<node html>', 
	 *   'format':'A4|Letter|Legal|...', 'orientation':'portrait|landscape', 'margin':'<margin value>',
	 *   'type':'html|snapshot'}.
	 */
	html2pdf : function(node, args) {
		if (!node) {
			LOGGER.warn('Missing node arg');
			return;
		}
		args = args || {};
		args['app'] = args['app'] || this._curPage['sys'];
		args['type'] = args['type'] || 'html';
		args['format'] = args['format'] || 'Letter';
		args['orientation'] = args['orientation'] || 'Landscape';
		
		var html = args['type'] == 'snapshot' ? domvas.toHtml(node[0]) : node.clone().html();
		args['html'] = html.replace(/\t/g, "");

		this._reqh2p = AJAX.call('html2pdf.ajax', 
			{'download':true, 'service':'html2pdf'},
			[args], function(result) {
				if (!AJAX.isSuccess(result, this._reqh2p)) {
					return;
				}
			});
		}
	}
);


/**
 * @class AppLoader
 * @memberof portal
 * @description Loads all the modules of an application.
 *
 * The AppLoader aggregates all the JavaScript modules in order to help the boot of each butor application.
 * It uses the {@link butor.Loader Loader} object in order to load all the **javascript** dependencies
 * of your UI componant.
 * It fires the events {@link portal.AppLoader#event:ready ready} and {@link portal.AppLoader#event:load load}
 * in order to load all your dependencies in the right order and to throws the launch of your application.
 *
 * @param {string} appId - ID of the application to load
 * @param {string[]} modules - List of paths for all modules to load (production loading)
 * @param {string[]} debugModules - List of paths for all modules to load (development/test loading)
 *
 * @fires portal.AppLoader#ready
 * @fires portal.AppLoader#load
 *
 * @example
 * butor.example.AppLoader = butor.example.AppLoader || portal.AppLoader.define({
 * 	_numOfLoadedModules: 0;
 * 	construct : function() {
 *		var context = '/go/to/js/path/'; // Context prefix in order to get each path of each module
 *		var debugModules = [ // List of mudules used in debug mode
 *			context + 'example.js',
 *			context + 'bundles.js'
 *		];
 *
 *		var modules = [context + 'example.min.js'];
 *		this.base('example', modules, debugModules);
 *
 *		this.onReady(function() {
 *			App.addBundle('example', butor.example.bundles);
 *			butor.example.myExample = new butor.example.MyExample();
 *		});
 *
 *		this.onLoad($.proxy(function() {
 *			this._numOfLoadedModules++;
 *			// Could be used into a progress bar...
 *		}, this));
 * 	}
 * });
 */
portal.AppLoader = portal.AppLoader || butor.Class.define({
	_ready : false,
	_loaded : false,
	_appId : null,
	_modules : null,
	_debugModules : null,
	properties : {
		/**
		 * @member {boolean} [debugMode=false] - Determines if debug modules are loaded instead of production modules.
		 * @memberof portal.AppLoader#
		 */
		'debugMode' : {
			type : 'boolean'
		}
	},
	construct : function(appId, modules, debugModules) {
		_appId = appId;
		_modules = modules;
		_debugModules = debugModules;
	},
	/**
	 * @method onLoad
	 * @description Calls the function parameter each time a module is loading.
//	 * @memberof portal.AppLoader#
	 * @param {function} callback - The function to call.
	 */
	onLoad : function(callback) {
		this.bind('loaded', callback);
		if (this._loaded) {
			callback();
			this.unbind('loaded', callback);
		}
	},
	/**
	 * @method onReady
	 * @description Calls the function parameter when all modules are loaded.
	 * @memberof portal.AppLoader#
	 * @param {function} callback - The function to call.
	 */
	onReady : function(callback) {
		this.bind('ready', callback);
		if (this._ready) {
			callback();
			this.unbind('ready', callback);
		}
	},
	/**
	 * @event ready
	 * @description Notifies the end of the loading of all modules.
	 * @memberof portal.AppLoader#
	 */
	ready : function() {
		this._ready = true;
		this.fire('ready', null);
		this.unbindAll('ready');
	},
	/**
	 * @event load
	 * @description Notifies each module loading.
	 * @memberof portal.AppLoader#
	 */
	load : function() {
		var loader = new butor.Loader();
		loader.bind('done', $.proxy(function (e) {
			this._loaded = true;
			this.fire('loaded', null);
			this.unbindAll('loaded');
			this.unbindAll('loadingScript');
		}, this));
		loader.bind('loadingScript', $.proxy(function (e) {
			this.fire('loadingScript', e.data);
		}, this));

		loader.loadScripts(
			(this.isDebugMode() ? _debugModules : _modules),
			_appId);
	}
});
//TODO tempo - to be removed
butor.AppLoader = portal.AppLoader;

/**
 * @class NotifManager
 * @memberof portal
 * @description
 * Notification manager for the portal application.
 */
portal.NotifManager = butor.Class.define({
	_lastNotifTime : null,
	_nReqId : null,
	_opened : false,
	_timer : null,
	_websocket : null,
	_subscriptions : {},
	construct : function() {
		
	},

	/**
	 * @method _getWsUrl
	 * @memberof portal.NotifManager#
	 * @access private
	 */
	_getWsUrl : function(ws) {
        var l = window.location;
        return ((l.protocol === "https:") ? "wss://" : "ws://") +
        	l.hostname + ((!butor.Utils.isEmpty(l.port) && (l.port != 80) && (l.port != 443)) ? ":" + l.port
                : "") + l.pathname + ws;
    },

	/**
	 * @method start
	 * @memberof portal.NotifManager#
	 * Start the NotifManager by creating a websocket.
	 */
	start : function() {
		// Ensures only one connection is open at a time
		if (this._websocket && this._websocket.readyState !== WebSocket.CLOSED) {
			LOGGER.info("websocket is already opened.");
			return;
		}

		if (!this._timer) {
			this._timer = new butor.Timer($.proxy(this._checkSanity, this), 2000);
		}
		
		this._lastNotifTime = new Date().getTime();

		App.ping($.proxy(function(echoFromBackend) {
			if (!echoFromBackend) {
				return;
			}
			this._websocket = new WebSocket(this._getWsUrl("websocket/notif"));

			this._websocket.onopen = $.proxy(function(event) {
				LOGGER.info("notif websocket openned");
				this._opened = true;
				
				setTimeout($.proxy(function() {
					for (var type in this._subscriptions) {
						this.send({'type':'subscribe', 'name':type});
					}
				}, this), 200);

				if (event.data) {
					this.onMessage(event.data);
				}
			}, this);

			this._websocket.onmessage = $.proxy(this.onMessage, this);

			this._websocket.onclose = $.proxy(function(event) {
				this._opened = false;
				this._websocket = null;
				LOGGER.info("notif websocket closed");
			}, this);

			this._timer.start();
		}, this));
	},

	/**
	 * @method send
	 * @memberof portal.NotifManager#
	 * @description
	 * Send a message to the server.
	 * @param {String} msg - The message to send.
	 */
	send : function(msg) {
		if (!this._opened) {
			return;
		}
		try {
			var data = JSON.stringify(msg);
			LOGGER.info('Sending websocket msg: ' +data)
			this._websocket.send(data);
		} catch (err) {
			LOGGER.error('Failed to send websocket msg! ' +err);
		}
	},

	/**
	 * @method stop
	 * @memberof portal.NotifManager#
	 * @description
	 * Stop the NotifManager.
	 */
	stop : function() {
		if (!this._opened) {
			return;
		}
		LOGGER.info("closing notif websocket ...")
		this._websocket.close();
		this._subscriptions = {};
	},

	/**
	 * @method load
	 * @memberof portal.NotifManager#
	 * @access private
	 */
	_checkSanity : function() {
		if (new Date().getTime() -this._lastNotifTime > 90000) {
			this.stop();
			setTimeout($.proxy(this.start, this), 300);
		}
	},

	/**
	 * @method subscribe
	 * @memberof portal.NotifManager#
	 * @description
	 * Subscribe to an event on the server.
	 * @param {String} type - Type of the subscription.
	 * @param {Function} callback - Handler binded with the event
	 */
	subscribe : function(type, callback) {
		this.bind(type, callback);

		if (!this._opened) {
			this.start();
		}
		this._subscriptions[type] = type;
		this.send({'type':'subscribe', 'name':type});
	},

	/**
	 * @method unsubscribe
	 * @memberof portal.NotifManager#
	 * @description
	 * Unsubscribe to an event on the server.
	 * @param {String} type - Type of the event to unsuscribe.
	 * @param {Function} callback - Handler binded with the event.
	 */
	unsubscribe : function(type, callback) {
		this.unbind(type, callback);
		this.send({'type':'unsubscribe', 'name':type});
		delete this._subscriptions[type];
	},

	/**
	 * @method onMessage
	 * @memberof portal.NotifManager#
	 * @description
	 * Fires an event with the specified message data (can be used to keep a notification alive after a fire of an event).
	 * @param {Map} msg - The message to re-fire. The message is composed mainly of a *type* and a *data* properties. If the data is equal to 'keep-alive', the notification is resended in order to keep it alive.
	 */
	onMessage : function(msg) {
		this._lastNotifTime = new Date().getTime();
		try {
			var data = msg.data;
			if (data == 'keep-alive') {
				LOGGER.info('got notif keep-alive');
				this.fire(data, {'type':'keep-alive', 'data':{'time':this._lastNotifTime, 'msg':''}});
				return;
			}
			var notif = jQuery.parseJSON(data);
			this.fire(notif.type, notif);
		} catch (err) {
			LOGGER.error('Failed on processing websocket msg! ' +err);
		}
	}
});

/**
 * @class ProfileProvider
 * @description Provider of the user profile.
 * @singleton
 * @memberof portal
 */
portal.ProfileProvider = function() {
	return {
		/**
		 * @method load
		 * @memberof portal.ProfileProvider#
		 * @param {Function} [doneCallback] - Callback when the AJAX request is done.
		 */
		'load' : function(doneCallback) {
			AJAX.call('portal.ajax',
				{'streaming':false, 'service':'getProfile'},
				[], function(result) {
					if (result && !result['hasError'] && result['data'] && result['data'][0]) {
						if (typeof(doneCallback) == 'function') {
							doneCallback(result['data'][0]);
						}
					} else {
						if (typeof(doneCallback) == 'function') {
							doneCallback(null);
						}
					}
				});
		},

		/**
		 * @method update
		 * @memberof portal.ProfileProvider#
		 * @param {String} key - Key of the data to update.
		 * @param {String} value - Value of the data to update.
		 */
		'update' : function(key, val) {
			AJAX.call('portal.ajax',
				{'streaming':false, 'service':'updateProfileAttr'},
				[key, val], function(result) {
						// OK
				});
		},

		/**
		 * @method remove
		 * @memberof portal.ProfileProvider#
		 * @param {String} key - Key of the data to remove.
		 */
		'remove' : function(key) {
			AJAX.call('portal.ajax',
				{'streaming':false, 'service':'deleteProfileAttr'},
				[key], function(result) {
						// OK
				});
		}
	};
};

/**
 * @class UserProfile
 * @memberof portal
 * @description Wrapper of the user profile.
 * @param {Map} [user={}] - Default user.
 */
portal.UserProfile = butor.Class.define({
	construct : function(props) {
		this._props = props || {};
		this._provider = null;
	},

	/**
	 * @method setProvider
	 * @memberof portal.UserProfile#
	 * @description Set the user provider.
	 * @param {Provider} provider - The user provider.
	 * @see portal.ProfileProvider
	 */
	setProvider : function(provider) {
		this._provider = provider;
	},

	/**
	 * @method load
	 * @memberof portal.UserProfile#
	 * @description Load the user profile from the provider.
	 * @fires portal.PortalApp#profileLoaded
	 */
	load : function() {
		this._provider.load($.proxy(function(props) {
			this._props = props || {};
			this.fire('profileLoaded');
		}, this));
	},

	/**
	 * @method set
	 * @memberof portal.UserProfile#
	 * @description Set a new entry in the user profile.
	 * @param {String} key - The key of the entry.
	 * @param {String} value - The value of the entry.
	 */
	set : function(key, val) {
		this._props[key] = val;
	},

	/**
	 * @method save
	 * @memberof portal.UserProfile#
	 * @description Save a specific entry of the profile.
	 * @param {String} key - The key of the entry.
	 * @param {String} value - The value of the entry.
	 */
	save : function(key, val) {
		this.set(key, val);
		this._provider.update(key, val);
	},

	/**
	 * @method get
	 * @memberof portal.UserProfile#
	 * @description Get a value of the profile.
	 * @param {String} key - The corresponding key for the value.
	 * @param {String} default - The default value if there is not corresponding value.
	 * @return {String} The value to get.
	 */
	get : function(key, dflt) {
		var val = this._props[key];
		if (val === undefined) {
			val = dflt;
		}
		return val;
	},

	/**
	 * @method remove
	 * @memberof portal.UserProfile#
	 * @description Remove an entry of the profile.
	 * @param {String} key - The key of the entry to remove.
	 */
	remove : function(key) {
		if (!this._props[key]) {
			return;
		}
		delete this._props[key];
		this._provider.remove(key);
	}
});

portal['support'] = function() {
	var generateSqlQuery = function(sys, sqlCall, callback) {
		//Double Escape. One for java and one for sql
		// So the displayed sql could be copy/paste directly in a sql console.
		sqlCall = sqlCall.replace(/\\\"/g,"\\\\\\\"");
		var url = sys +'.support.ajax';
		if (sys != 'portal') {
			url = '/' +sys +'/' +url;
		}
		AJAX.call(url,
			{'streaming':false, 'service':'generateSqlQuery'},
			[sqlCall], callback);
	};
	return {
		generateSqlQuery : generateSqlQuery
	};
}();

var _app = new portal.PortalApp();

// define/override App singleton
App = _app;

/* BO backward compatibility glue */

App.Bundle = {
	'add' : $.proxy(App.addBundle, App),
	'get' : $.proxy(App._bundle.get, App._bundle),
	'override' : $.proxy(App._bundle.override, App._bundle)
};

MsgPanel = butor.panel.MsgPanel;

App.isEmpty = $.proxy(butor.Utils.isEmpty, butor.Utils);
App.formatDate = butor.Utils.formatDate;
App.parseDate = butor.Utils.parseDate;
App.isInteger = butor.Utils.isInteger;
App.isFloat = butor.Utils.isFloat;

App.events = {
	'bind' : $.proxy(App.bind, App),
	'fire' : $.proxy(App.fire, App),
	'unbind' : $.proxy(App.unbind, App)
};
App.Utils = butor.Utils;
App.AttrSet = new butor.AttrSet();

/* EO backward compatibility glue */

_app.addBundle('common', butor.bundles);
_app.addBundle('common', {
	'Welcome': {'fr' : 'Bienvenue'},
	'Company': {'fr' : 'Companie'},
	'Firm': {'fr' : 'Firme'},
	'Note': {'fr' : 'Note'},
	'Client': {'fr' : 'Client'},
	'Name': {'fr' : 'Nom'},
	'Active': {'fr' : 'Active'},
	'Country': {'fr' : 'Pays'},
	'Currency': {'fr' : 'Devise'},
	'Calendar': {'fr' : 'Calendrier'},
	'Quantity': {'fr' : 'Quantité'},
	'Fund': {'fr' : 'Devise'},

	'At': {'fr' : 'À'},
	'First name': {'fr' : 'Prénom'},
	'Last name': {'fr' : 'Nom'},
	'Display name': {'fr' : 'Nom complet'},

	'Status' : {'fr': 'Statut'},
	'Date, from' : {'fr': 'Date, de'},
	'Field' : {'fr': 'Champs'},
	'Updates' : {'fr': 'M.À.J.'},
	'Comment' : {'fr': 'Commentaire'},
	'Yes' : {'fr': 'Oui'},
	'No' : {'fr': 'Non'},
	"User's guide" : {'fr': "Guide d'usager"},
	'Manage API Key' : {'fr': 'Gérer clé API'},
	'Password' : {'fr':'Mot de passe'},
	'pwdExpireSoon' : {'en' : 'Your password will expire in _days_ day(s).<br><br><a href="#" id="popChangePwdLink">Change it now</a><br><br><a href="#" id="dismissPopChangePwdLink">Later</a>',
		'fr' : 'Votre mot de passe expirera dans _days_ jour(s).<br><br><a href="#" id="popChangePwdLink">Changer le maintenant</a><br><br><a href="#" id="dismissPopChangePwdLink">Plus tard</a>'},

	'Open in a new window': {'fr' : 'Ouvrir dans une nouvelle fenêtre'},
	'Are you sure you want to quit?': {'fr' : 'Êtes vous certain de vouloir quitter?'},
	'Set as start page': {'fr' : 'Retenir comme page de départ'},
	'Contact us': {'fr' : 'Nous rejoindre'},
	'USER_ID_SHOULD_BE_EMAIL': {'en' : 'User ID should be an email',
		'fr': "L'identifiant d'un usager doit être un courriel"},
	'UNAUTHORIZED' : {'en' : 'Access denied', 'fr' : 'Accès refusé'},
	'USER_NOT_FOUND' : {'en' : 'User not found', 'fr' : 'Usager introuvable'},
	'FIRM_NOT_FOUND' : {'en' : 'Firm not found', 'fr' : 'Firme introuvable'},
	"USER_SHOULD_BE_INACTIVE_TO_BE_DELETED": {'en':'User should be inactive to be deleted',
		'fr': "Pour supprimer un usager, il doit être inactif"},
	"FIRM_SHOULD_BE_INACTIVE_TO_BE_DELETED" : {'en':'Firm should be inactive to be deleted',
		'fr': "Pour supprimer une firme, elle doit être inactive"},
	"FIRM_SHOULD_NOT_HAVE_USERS_TO_BE_DELETED" : {'en': 'Firm sould not have users to be deleted',
		'fr': "Pour supprimer une firme, elle ne doit pas avoir d'usagers"}
});

_app['notif'] = new portal.NotifManager();

$.getScript("/wl?j=1").done(function() {
	_app.start();
}).fail(function() {
	_app.start();
});

/**
 * Notify that the setup of the **PortalApp** is done.
 * @event portal.PortalApp#setupDone
 */

/**
 * Notify that the user profile is loaded.
 * @event portal.PortalApp#profileLoaded
 */

/**
 * Notify that the history has changed.
 * @event portal.PortalApp#historyChanged
 * @type {Map}
 * @prop {String} [t] - Type of the history.
 * @prop {String} [p] - Page to load.
 * @prop {Map} [args] - Arguments for the page to load.
 */

/**
 * Notify that the language has changed.
 * @event portal.PortalApp#langChanged
 */

/**
 * Notify that the navbar is removed.
 * @event portal.PortalApp#removing-navbar
 * @prop {String} sysId - ID of the system.
 */

 /**
  * Notify that a page is removed.
  * @event portal.PortalApp#removing-page
  * @prop {String} pageId - ID of the removed page.
  */

	/**
	 * Notify that a page is opened.
	 * @event portal.PortalApp#opening-page
	 * @prop {String} pageId - ID of the opened page.
	 */
//# sourceURL=butor.portal.js
