/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.portal.web.notif;

import javax.websocket.MessageHandler;
import javax.websocket.RemoteEndpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.butor.notif.AbstractNotifSession;
import com.google.common.base.Preconditions;

public class NotifSession extends AbstractNotifSession implements MessageHandler.Whole<String> {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private RemoteEndpoint.Basic remoteEndpointBasic;
	
	public NotifSession(String id, String username, RemoteEndpoint.Basic remoteEndpointBasic) {
		super(id, username);
		this.remoteEndpointBasic = Preconditions.checkNotNull(remoteEndpointBasic);
	}
	/**
	 * handle client notification coming via websocket
	 */
	@Override
    public void onMessage(String serializedMessage) {
		super.handleClientMessage(serializedMessage);
    }
	@Override
	public void sendToClient(String serializedMessage) {
		try {
			remoteEndpointBasic.sendText(serializedMessage);
		} catch (Exception e) {
			logger.warn("Failed while sending message to client!", e);
		}
	}
}
