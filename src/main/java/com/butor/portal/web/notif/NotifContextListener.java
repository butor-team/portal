/**
 * Copyright 2013-2018 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.portal.web.notif;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.butor.notif.DefaultNotifManager;
import com.butor.notif.NotifManager;

public final class NotifContextListener implements ServletContextListener {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private static DefaultNotifManager notifManager = null;
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		try {
			WebApplicationContext ctxt = WebApplicationContextUtils.getWebApplicationContext(sce.getServletContext());
			notifManager = ctxt.getBean(DefaultNotifManager.class);
		} catch (NoSuchBeanDefinitionException e) {
			logger.warn("Failed while retrieving notification manager bean! Fix if you expecting notification to be enbaled!", e);
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		if (notifManager != null) {
			notifManager.shutdown();
		}
	}
	
	public static NotifManager getNotifManager() {
		return notifManager;
	}
}
